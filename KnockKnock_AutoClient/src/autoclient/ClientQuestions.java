package autoclient;

public class ClientQuestions {
	
	private static final int READY = 0;
	private static final int SENT_GENERIC_WHO = 1;
	private static final int SENT_SPECIFIC_WHO = 2;
	
	private int count;
	private int state;
	
	public ClientQuestions (){
		this.count = 0;
		this.state = READY;
	}
	
	//Calcola il nuovo stato e restituisce la prossima stringa da inviare al
	//server. Restituisce 'null' in caso di errore
	public String nextQuestion(String serverInput){
		String nextQ = null;
		
		if(serverInput == null)
			return null;
		
		switch (this.state){
			case READY : {
				if(serverInput.equals("Knock! Knock!")){
					this.state = SENT_GENERIC_WHO;
					nextQ = "Who's there?";
				}
				break;
			}
			
			case SENT_GENERIC_WHO : {
				this.state = SENT_SPECIFIC_WHO;
				nextQ = serverInput + " who?";
				break;
			}
			
			case SENT_SPECIFIC_WHO : {
				this.count++;
				
				this.state = READY;
				
				if(this.count > 4)
					nextQ = "n";				
				else
					nextQ = "y";
				
				break;
			}
			
			default : break;
		}
		
		return nextQ;
	}
	
	public void restart(){
		this.state = READY;
		this.count = 0;
	}
}
